/* 
* Clock.js
*
* Functions for Web-Media midterm
*
* Joey Idler
*/

function digiClock() {

	var time = new Date();

	// variables for time 
	var hour = time.getHours();
	var minute = time.getMinutes();
	var seconds = time.getSeconds();
	
	// variables for the date
	var day = time.getDate();
	var month = time.getMonth();
	var year = time.getFullYear();

	document.getElementById('txt').innerHTML = 
		timeFormat(hour, minute, seconds) + " " +
		dateFormat(day, month, year);
	var timeout = setTimeout(digiClock, 500);

}

function timeFormat(h, m, s) {

	h = (h < 10) ? "0" + h : h;
	m = (m < 10) ? "0" + m : m;
	s = (s < 10) ? "0" + s : s;

	return h + ":" + m + ":" + s;
}

function dateFormat(d, m, y) {

	var month = ["January", "February", "March",
		     "April", "May", "June", 
		     "July", "August", "September",
		     "October", "November", "December"];
	return month[m] + " " + d + ", " + y;
}

/*
function date() {

	var time = new Date();

	// variables for the date
	var day = time.getDate();
	var month = time.getMonth();
	var year = time.getFullYear();

}
*/
