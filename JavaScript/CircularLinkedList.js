// CircularLinkedList.js
//
// Joey Idler

// CLLNode(value) is a constructor for circular linked list node.
// It initializes its value to the given argument.
function CLLNode(value) {

  this.previous = this.next = null;
  this.value = value;

  this.isHead = 
    function isHead() {
      return (this.value == null);
    };
}

// Constructor for CircularLinkedList objects
function CircularLinkedList() {

  this.head = new CLLNode(null);
  this.head.previous = this.head.next = this.head;


  this.insert = 
   function insert(value) {

       var node = new CLLNode(value);

       node.next = this.head;
       node.previous = this.head.previous;
       node.previous.next = node;
       this.head.previous = node;

//       this.printList();
   };

   this.remove = 
    function remove(value) {
       var node = this.search(value);

       if(node != null) {

         node.next.previous = node.previous;
	 node.previous.next = node.next;
       }

//       this.printList();
    };

    this.search = 
     function search(value) {

       var node = this.head.next;

       while(node.value != null) {

         if(node.value == value)
	   return node;

	 node = node.next;
       }

       return null;
     };


     // Helper function to check for errors
     // Prints values for each node in the linked list
     this.printList = 
      function printList() {

        var node = this.head.next;
	do {
 
          window.alert("Node value = " + node.value);
          node = node.next;

	} while(node.value != null)
      };
}

// Create Circular Linked List and insert values
var cll = new CircularLinkedList();
cll.insert(4);
cll.insert(12);
cll.insert(42);
cll.insert(0);
cll.insert(12);

// Search linked list
window.alert("Contains 12? " + (cll.search(12) != null));
window.alert("Contains 42? " + (cll.search(42) != null));
window.alert("Contains 1? " + (cll.search(1) != null));
window.alert("Contains 22? " + (cll.search(22) != null));

// remove from linked list
cll.remove(42);

window.alert("Removed 42 from linked list");

// Check if 42 has been removed
window.alert("Contains 42? " + (cll.search(42) != null));


