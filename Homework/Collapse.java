/* Collapse.java
*
* 
* Joey Idler
*/

import org.w3c.dom.*;

public void makeCollapsible(String elementId) {

	Element element = document.getElementById(elementId);
	if(element) {
		Element div = document.createElement("div");
		element.getParentNode().insertBefore(div, element);
		Element button = document.createElement("button");
		div.appendChild(button);
		button.setAttribute("type", "button");
		Text buttonText = document.createTextNode("click to collapse");
		button.appendChild(buttonText);
		buton.setAttribute("onclick",
				"toggleVisibility(this, '" + elementId + "');");

	}
}

