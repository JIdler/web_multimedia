<?xml version="1.0" encoding="UTF-8"?>

<xsl:transform version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml">
<!--
  xml:space='preserve'>
-->
  <!-- the space causes an error on the second template because
       xsl:attribute must come before first child node and space
       preservation creates a white space text node... -->
  <xsl:template match="/rss">
    <html>
      <head>
        <title>
          RSStoXHTML2
        </title>
      </head>
      <body>
        <h1>
          <xsl:value-of select="channel/title" /> 
          RSS feed links:
        </h1>
        <ul>
          <xsl:apply-templates select="channel/item" />
        </ul>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="item">
    <li>
      <a>
        <xsl:attribute name="href">
          <xsl:value-of select="link" />
        </xsl:attribute>
        <xsl:attribute name="title">
          <xsl:value-of select="description" />
        </xsl:attribute>
        <xsl:value-of select="title" />
      </a>
    </li>
  </xsl:template>

</xsl:transform>
