// JAXP classes
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

// DOM classes
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

// JDK classes
import java.io.File;

/** Input an RSS document, remove the first "item" element, and
    output the resulting RSS document to System.out */
class DOMtoText {
    public static void main(String args[]) {

        try {
            // Input an RSS document into a DOM Document object
            DocumentBuilderFactory docBuilderFactory = 
                DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = docBuilderFactory.newDocumentBuilder();
            Document document = parser.parse(new File(args[0]));
            
            // Use the DOM API to remove the first item element
            // (this code assumes that there is at least one item...)
            NodeList items = document.getElementsByTagName("item");
            items.item(0).getParentNode().removeChild(items.item(0));

            // Use JAXP methods to output the modified Document object
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer();
            transformer.transform(new DOMSource(document),
                                  new StreamResult(System.out));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }
}
