// JAXP classes
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

// DOM classes
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

// JDK classes
import java.io.File;

/** Count the number of link elements in an RSS 0.91 document */ 
class DOMCountLinks {

    /** Main program does it all */
    static public void main(String args[]) {
        try {
            // JAXP-style initialization of DocumentBuilder
            // (XML parser that builds DOM from document)
            DocumentBuilderFactory docBuilderFactory = 
                DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = docBuilderFactory.newDocumentBuilder();
            
            // Parse XML document from file given by first argument
            // into a DOM Document object
            Document document = parser.parse(new File(args[0]));

            // Process the Document object using the Java API version of
            // the W3C DOM 
            NodeList links = document.getElementsByTagName("link");
            System.out.println("Input document has " +
                               links.getLength() +
                               " 'link' elements.");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }
}
