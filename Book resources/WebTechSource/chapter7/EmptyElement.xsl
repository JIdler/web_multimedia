<?xml version="1.0" encoding="UTF-8"?>

<xsl:transform version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml">

  <xsl:template match="/">
    <html>
      <head>
        <title>
          EmptyElement
        </title>
      </head>
      <body>
        <ul>
          <li> </li>
          <li><xsl:text> </xsl:text></li>
          <li>
            <xsl:text 
              disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </li>
          <!-- The following causes an error, because XSLT does not
          define a &nbsp; entity:
          -->
          <!--
          <li>
            <xsl:text 
              disable-output-escaping="yes">&nbsp;</xsl:text>
          </li>
          -->
        </ul>
      </body>
    </html>
  </xsl:template>
</xsl:transform>
