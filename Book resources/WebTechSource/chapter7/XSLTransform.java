// Based on http://xml.apache.org/xalan-j/usagepatterns.html

// JAXP classes
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;

// JDK classes
import java.io.File;

/** Apply the XSL transform contained in the file named by
    the first command-line argument to the XML document
    named by the second argument and write the resulting
    document to standard output. */
class XSLTransform {
    public static void main(String args[]) {

        try {
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = 
                tFactory.newTransformer(
                    new StreamSource(new File(args[0])));
            transformer.transform(
              new StreamSource(new File(args[1])), 
              new StreamResult(System.out));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }
}
    
