<?xml version="1.0" encoding="UTF-8"?>

<xsl:transform version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml">

  <xsl:template match="/rss">
    <html>
      <head>
        <title>
          RSStoXHTML
        </title>
      </head>
      <body>
        <h1>
          <xsl:value-of select="channel/title" /> 
          RSS feed links:
        </h1>
        <ul>
          <xsl:apply-templates select="channel/item" />
        </ul>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="item">
    <li>
      <xsl:value-of select="title" />
    </li>
  </xsl:template>

</xsl:transform>
