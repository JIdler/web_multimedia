<?xml version="1.0" encoding="UTF-8"?>

<xsl:transform version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
  xml:space="preserve"
  >

  <xsl:output
    method="xml"
    version="1.0"
    encoding="UTF-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    omit-xml-declaration="yes"
  />

  <xsl:template match="/rss">
    <html>
      <head>
        <title>
          RSStoXHTML3
        </title>
      </head>
      <body>
        <h1>
          <xsl:value-of select="channel/title" /> 
          RSS feed links:
        </h1>
        <ul>
          <xsl:apply-templates select="channel/item" />
        </ul>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="item">
    <li>
      <a href="{link}" 
        title="{normalize-space(string(description))}">
        <xsl:value-of select="title" />
      </a>      
    </li>
  </xsl:template>

</xsl:transform>
