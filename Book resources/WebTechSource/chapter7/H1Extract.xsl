<?xml version="1.0" encoding="UTF-8"?>

<xsl:transform version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns="http://www.w3.org/1999/xhtml">

  <xsl:template match="/xhtml:html">
    <html>
      <head>
        <title>
          H1Extract
        </title>
      </head>
      <body>
        <xsl:apply-templates select=".//xhtml:h1" />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="xhtml:h1">
    <xsl:copy-of select="." />
  </xsl:template>

</xsl:transform>
