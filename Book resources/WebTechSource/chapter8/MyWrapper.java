import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class MyWrapper extends HttpServletRequestWrapper {

    /** Construct a ``wrapper'' of the given request that
     *  behaves exactly like the original request except
     *  for any methods this class overrides.
     */
    public MyWrapper(HttpServletRequest request) {
        super(request);
    }

    /** Override the original request's getParameter()
     *  method to simulate the addition of a parameter named
     *  "special".
     */
    public String getParameter(String paramName) {
        String paramValue = null;
        if (paramName.equals("special")) {
            paramValue = "You Win!";
        } else {
            paramValue = super.getParameter(paramName);
        }
        return paramValue;
    }

    /* Should also override getParameterNames() and
     * getParameterValues(); see text. */
}
