import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Servlet acting as MVC controller in a very simple application.
 */
public class Controller extends HttpServlet
{ 
    /**
     * If session is new then increment and display the application
     * visit counter.  Otherwise (this is the continuation of an
     * active session), display a message.
     */
    public void doGet (HttpServletRequest request,
                       HttpServletResponse response) 
        throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        if (session.isNew()) {
            RequestDispatcher visitDispatch =
                getServletContext().getNamedDispatcher("visit_count");
            visitDispatch.forward(request, response);
        }
        else {
            RequestDispatcher laterDispatch = 
                getServletContext().getNamedDispatcher("visit_later");
            laterDispatch.forward(request, response);
        }
    }
}
