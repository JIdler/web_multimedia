This directory and its subdirectories represents a
complete web application.

To run this web application

1. Locate the JWSDP 1.3 installation directory, which contains a
   number of directories including conf and webapps.
2. Copy this directory and all of its contents (including
   subdirectories) to the webapps directory of your JWSDP 1.3 
   installation.
3. Log in to the JWSDP Web App Manager at 
   http://localhost:8080/manager/html.
4. If /MyOwnBlog does not appear in the Path column of the list
   of applications, deploy the application by typing
   MyOwnBlog in the "WAR or Directory URL" text box and clicking
   the Deploy button immediately below the box.
5. Browse to http://localhost:8080/MyOwnBlog/login.html and
   login with user name nice, password try 
6. Happy blogging!
