import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.Calendar;
import java.util.Vector;
import org.xml.sax.SAXParseException;


/**
 * Controller for MVC-based implementation of My Own Blog!
 * web application.
 */
public class MyOwnBlog extends HttpServlet
{
    /**
     * Initialize the data store when this component is instantiated.
     */
    public void init()
    {
        try {
            // Initialize the data store
            DataStore.initialize();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Process HTTP GET requests.  This involves displaying
     * the blog either for the current month or for a month
     * specified via parameter values.
     */
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
        throws ServletException, IOException
    {
        // From what month and year should blog entries be displayed?
        // If there is no "month" or "year" parameter present in the
        // request, use the current month and year.  Otherwise, decode
        // month/year information from the appropriate parameter.
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1;  
        String monthParam = request.getParameter("month");
        if (monthParam != null) {
            try {
                month = Integer.parseInt(monthParam);
            }
            catch (NumberFormatException nfe) {
                // Use default month if this fails
            }
        }
        String yearParam = request.getParameter("year");
        if (yearParam != null) {
            try {
                year = Integer.parseInt(yearParam);
            }
            catch (NumberFormatException nfe) {
                // Use default year if this fails
            }
        }
        
        // Retrieve the appropriate entries from the data store
        // and add to the request
        Vector entries = DataStore.getEntries(month, year);
        request.setAttribute("entries", entries);

        // Retrieve months for which blog entries are available
        // and add to the request
        Vector months = DataStore.getAllMonths();
        request.setAttribute("months", months);

        // Forward this request on to the DisplayBlog component.
        RequestDispatcher viewBlog = 
            getServletContext().getNamedDispatcher("DisplayBlog");
        viewBlog.forward(request, response);

        return;
    }

    /**
     * Process HTTP POST requests.  The type of request is
     * determined by pathInfo:
     * - Login: attempt to log user in
     * - AddOrPreview: depending on value of parameter,
     *   either produce preview of user's current entry
     *   or add an entry to the blog
     */
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response)
        throws ServletException, IOException
    {
        String option = request.getPathInfo();

        // Process login form 
        if (option.equals("/Login")) {
            String username = request.getParameter("username");
            String password = request.getParameter("pwd");
            if (username != null && password != null &&
                username.equals("nice") &&
                password.equals("try")) {
                HttpSession session = request.getSession();
                session.setAttribute("loggedIn", new Boolean(true));
                response.sendRedirect("/MyOwnBlog/addentry.html");
            }
            else {
                response.sendRedirect("/MyOwnBlog/login.html");
            }
        }

        // Process add-entry form
        else if (option.equals("/AddOrPreview")) {
            // If session does not contain "loggedIn" attribute,
            // user is not logged in (session may have expired).
            // Redirect to error page if not logged in.
            HttpSession session = request.getSession();
            if (session.getAttribute("loggedIn") == null) {
                response.sendRedirect("/MyOwnBlog/loginRequired.html");
            }

            // Otherwise, add a new entry to the blog and display
            // the updated home page.
            else {

                try {
                    // Create a new entry.
                    Entry entry = 
                        new Entry(request.getParameter("title"),
                                  request.getParameter("entry"));

                    // If this is not a preview, then add entry to
                    // data store and display updated blog.
                    String doPreview = 
                        request.getParameter("doPreview");
                    if (doPreview != null && 
                        doPreview.equals("false")) {
                        DataStore.addEntry(entry);
                        response.sendRedirect("/MyOwnBlog/do");
                    }

                    // Otherwise, display only this entry.
                    else {
                        Vector oneEntry = new Vector();
                        oneEntry.add(entry);
                        request.setAttribute("entries", oneEntry);
                        // Do not define months attribute
                        RequestDispatcher viewBlog = 
                            getServletContext().
                              getNamedDispatcher("DisplayBlog");
                        viewBlog.forward(request, response);
                    }
                }
                catch (SAXParseException spe) {
                    request.setAttribute("SAXException", spe);
                    RequestDispatcher saxException = 
                        getServletContext().
                          getNamedDispatcher("SAXException");
                    saxException.forward(request, response);
                }
                catch (ServletException se) {
                    throw se;
                }
                catch (Exception e) {
                    throw new ServletException(e);
                }
            }
        }

        // Unrecognized option; just show the blog
        else {
            response.sendRedirect("/MyOwnBlog/do");
        }
    }

}
