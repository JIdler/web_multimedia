import java.sql.*;
public class MSConnect {
    
    public static void main(String[] args) {
        
        // change this to whatever your DSN is
        String dataSourceName = "PeopleDB";
        String dbURI = "jdbc:odbc:" + dataSourceName;
        try { 
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            Connection con = 
                DriverManager.getConnection(dbURI, "", "");

            // JDBC calls to con methods go here ...
            
            Statement stmt = con.createStatement();
            stmt.execute("CREATE TABLE PersonalInterests " +
                         "(Name VARCHAR(50), Interests VARCHAR(50))");
            stmt.execute("INSERT INTO PersonalInterests VALUES " +
                         "('Ben', 'drawing, Legos, writing')");
            System.out.println("Row count is " + stmt.getUpdateCount());
            stmt.execute("INSERT INTO PersonalInterests VALUES " +
                         "('Emily', 'piano, writing, reading')");
            stmt.execute("INSERT INTO PersonalInterests VALUES " +
                         "('Peter', 'writing songs, drums, politics')");
            stmt.execute("INSERT INTO PersonalInterests VALUES " +
                         "('Rebecca', 'people, music, reading')");
            stmt.execute("SELECT * FROM PersonalInterests");
            ResultSet rs = stmt.getResultSet();
            if (rs != null) {
                while (rs.next()) {
                    System.out.println(rs.getString("Name"));
                    System.out.println(rs.getString("Interests"));
                }
            }
            stmt.execute("DROP TABLE PersonalInterests");
            stmt.close();

            con.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
