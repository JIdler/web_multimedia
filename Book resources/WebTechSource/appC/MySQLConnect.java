import java.sql.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class MySQLConnect extends HttpServlet {

    public void doGet (HttpServletRequest request,
                       HttpServletResponse response) 
        throws ServletException, IOException
    {
        // Set the HTTP content type in response header
        response.setContentType("text/html; charset=\"UTF-8\"");
        
        // Obtain a PrintWriter object for creating the body
        // of the response
        PrintWriter servletOut = response.getWriter();
        
        // change this as appropriate 
        String host = "db.example.net:3306";
        String dbname = "People";
        String username = "someuser";
        String password = "mypassword";
        String dbURI = "jdbc:mysql:" + 
            "//" + host + 
            "/" + dbname +
            "?user=" + username +
            "&password=" + password;
        try { 
            // The newInstance() call is a work around for some 
            // broken Java implementations
            
            Class.forName("com.mysql.jdbc.Driver").newInstance(); 
            Connection con = 
                DriverManager.getConnection(dbURI);

            // JDBC calls to con methods go here ...

            Statement stmt = con.createStatement();
            stmt.execute("CREATE TABLE PersonalInterests " +
                         "(Name VARCHAR(50), Interests VARCHAR(50))");
            stmt.execute("INSERT INTO PersonalInterests VALUES " +
                         "('Ben', 'drawing, Legos, writing')");
            System.out.println("Row count is " + stmt.getUpdateCount());
            stmt.execute("INSERT INTO PersonalInterests VALUES " +
                         "('Emily', 'piano, writing, reading')");
            stmt.execute("INSERT INTO PersonalInterests VALUES " +
                         "('Peter', 'writing songs, drums, politics')");
            stmt.execute("INSERT INTO PersonalInterests VALUES " +
                         "('Rebecca', 'people, music, reading')");
            stmt.execute("SELECT * FROM PersonalInterests");
            ResultSet rs = stmt.getResultSet();
            if (rs != null) {
                while (rs.next()) {
                    System.out.println(rs.getString("Name"));
                    System.out.println(rs.getString("Interests"));
                }
            }
            stmt.execute("DROP TABLE PersonalInterests");
            stmt.close();

            con.close();
        }
        catch (Exception e) {
            servletOut.print(e);
        }
        finally {
            servletOut.close();
        }
    }
}
