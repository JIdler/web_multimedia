package myCurCon;

public interface CurCon extends java.rmi.Remote {
    public double[] fromDollars(double dollars) 
        throws java.rmi.RemoteException;
    public ExchangeValues fromEuros(double euros) 
        throws java.rmi.RemoteException;
    public ExchangeValues fromYen(double yen) 
        throws java.rmi.RemoteException;
}
