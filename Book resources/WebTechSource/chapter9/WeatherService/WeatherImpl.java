package myWeatherServer;

import java.math.BigDecimal;
import java.util.Calendar;
import java.rmi.RemoteException;

public class WeatherImpl implements NdfdXMLPortType {
    public String NDFDgen(BigDecimal latitude,
                          BigDecimal longitude,
                          ProductType product, 
                          Calendar startTime,
                          Calendar endTime,
                          WeatherParametersType weatherParameters) 
        throws RemoteException
    {
        // Return dummy string, ignoring parameter values
        String retVal =
            "<?xml version='1.0' ?> <dwml version='1.0' ... >" +
            "..." + "</dwml>";
        return retVal;
    }
}
