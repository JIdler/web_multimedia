package myCurCon;

public class CurConImpl implements CurCon {

    /* Exchange rates as of Dec 3, 2004 */
    private static final double dollar2euro = 0.746826;
    private static final double dollar2yen = 102.56;

    public ExchangeValues fromDollars(double dollars) 
        throws java.rmi.RemoteException
    {
        ExchangeValues ev = new ExchangeValues();
        ev.dollars = dollars;
        ev.euros = dollars * dollar2euro;
        ev.yen = dollars * dollar2yen;
        return ev;
    }

    public ExchangeValues fromEuros(double euros) 
        throws java.rmi.RemoteException
    {
        ExchangeValues ev = new ExchangeValues();
        ev.euros = euros;
        ev.dollars = euros / dollar2euro;
        ev.yen = ev.dollars * dollar2yen;
        return ev;
    }
        
    public ExchangeValues fromYen(double yen) 
        throws java.rmi.RemoteException
    {
        ExchangeValues ev = new ExchangeValues();
        ev.yen = yen;
        ev.dollars = yen / dollar2yen;
        ev.euros = ev.dollars * dollar2euro;
        return ev;
    }
}
