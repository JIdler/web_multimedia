package myCurConClient;

public class CurConBean {

    private double value = 1.0;
    private String currency = "dollars";

    public void setValue(double value) {
        this.value = value;
        return;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
        return;
    }
    public ExchangeValues getExValues() {
        ExchangeValues ev = null;
        CurCon curCon =
            (new HistoricCurrencyConverter_Impl()).getCurConPort();
        try {
            if (currency.equals("euros")) {
                ev = curCon.fromEuros(value);
            }
            else if (currency.equals("yen")) {
                ev = curCon.fromYen(value);
            }
            else {
                ev = curCon.fromDollars(value);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return ev;
    }
}
